#include "battery.h"

battery::battery() {
	Volt = 1.5F;
	Remaining = 10;
}
float battery::output(void) {
	if (Remaining >= 0) {
		Remaining -= Volt;
		if (Remaining < 0) {
			Remaining = 0;
		}
		return Volt;
	}
	else {
		return 0;
	}
}
void battery::remain(void) {
	std::cout << "バッテリー残量：" << Remaining << std::endl;
}
