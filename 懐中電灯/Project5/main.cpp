//ここに回路を組む
//必要な部品、電池、スイッチ、電球
#include "battery.h"
#include "Switch.h"
#include "lightbulb.h"
#include <stdio.h>
#include <iostream>

battery batteryA;
Switch switchA;
lightbulb lightbulbA;
float Elect;
bool Result = false;

int main(void) {
	for (int i = 0; i < 10; i++) {
		Elect = batteryA.output();
		if (Result == false) {
			switchA.turnswitch();
		}
		Elect = switchA.switchdetection(Elect);
		Result = lightbulbA.energydetection(Elect);
		//Result = lightbulbA.energydetection(switchA.switchdetection(Elect));
		if (Result == true) {
			std::cout << "電球が灯った" << std::endl;
		}
		else {
			std::cout << "電球は暗いままだ" << std::endl;
		}
		batteryA.remain();
	}
	return 0;
};
